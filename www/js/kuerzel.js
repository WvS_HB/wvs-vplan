/* 
 *   Copyright (C) by Timo Haumann, 2014
 *   This file is part of WvS-VPlan.
 *
 *   WvS-VPlan is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

window.lehrerKuerzel = {
    "Bal": "Frau Balhausen",
    "Bas": "Frau Baars",
    "Bks": "Frau Bunkers",
    "Brk": "Frau Brunke",
    "Bt": "Frau Bothe",
    "Bu": "Frau Buchholz-Borris",
    "Cl": "Herrn Carl",
    "Dt": "Herrn Daut",
    "Fö": "Herrn Förster",
    "Geb": "Frau Gebhardt",
    "Gli": "Frau Glittmann-Köhler",
    "Glö": "Herrn Glöge",
    "Gs": "Frau Garms",
    "Hbd": "Frau Haberland",
    "Hd": "Herrn Heiduk",
    "He": "Herrn Herrmann",
    "Ho": "Herrn Homut",
    "Hx": "Herrn Henrix",
    "Ja": "Frau Jacobi",
    "Kch": "Herrn Kachel",
    "Kgr": "Frau Krüger",
    "Kk": "Frau Kolakowski",
    "Klg": "Herrn Dr. Klingbeil",
    "Kr": "Herrn Kragesieber",
    "Kt": "Frau Klette",
    "Kü": "Frau Kühl",
    "Kun": "Herrn Kunze",
    "Lo": "Frau Lowin",
    "Ls": "Herrn Loschke",
    "Mar": "Frau Marquordt",
    "Me": "Herrn Mettler",
    "Mn": "Frau Meinecke",
    "Mtg": "Frau Mettge",
    "nBe": "Herrn Benz vom NIG",
    "nEr": "Herrn Ellner vom NIG",
    "Ni": "Frau Nikolai-Tschirner",
    "nKos": "Frau Korschorke vom NIG",
    "nKß": "Herrn Kreßmann vom NIG",
    "nSl": "Frau Schlüter vom NIG",
    "nSt": "Herrn Stehr vom NIG",
    "nSte": "Frau Stehr vom NIG",
    "nWe": "Herrn Weber vom NIG",
    "nWo": "Frau Wolf vom NIG",
    "Pei": "Herrn Peinemann",
    "Pf": "Frau Pfortner",
    "Ptz": "Frau Paetzold",
    "Pu": "Frau Puritz",
    "Rau": "Frau Rau",
    "Rei": "Herrn Dr. Reichert",
    "Rg": "Herrn Rettberg",
    "Rh": "Frau Rahlfs",
    "Ri": "Frau Ritscher",
    "Rö": "Herrn Knorre-Röhlich",
    "Rog": "Frau Rogge",
    "Sb": "Herrn Schwab",
    "Scha": "Frau Scharf",
    "Se": "Frau Strese",
    "Sel": "Herrn Scheel",
    "Sh": "Herrn D. Scholz",
    "Sm": "Frau Samel",
    "Sot": "Herrn Schott",
    "Spa": "Frau Spataro",
    "Sr": "Herrn Schüller",
    "Stk": "Frau Staniek",
    "Stp": "Frau Steppan",
    "Stt": "Herrn Stettin",
    "Sül": "Frau Schüler",
    "Sw": "Frau Salzwedel",
    "Sz": "Herrn M. Scholz",
    "Tt": "Frau Tust",
    "Wb": "Herrn Willbrandt",
    "Wer": "Frau Meerlender-Werner",
    "Wf": "Frau Wolf",
    "Wi": "Herrn Wittig",
    "Woj": "Frau Wojan",
    "Wzb": "Herrn Wurzbacher",
    ".": "(nicht festgelegt)"
};

window.fachKuerzel = {
    "EN": "Englisch",
    "PH": "Physik",
    "GE": "Geschichte",
    "FR": "Französisch",
    "MA": "Mathematik",
    "DE": "Deutsch",
    "CH": "Chemie",
    "SP": "Sport",
    "EK": "Erdkunde",
    "LA": "Latein",
    "MU": "Musik",
    "BI": "Biologie",
    "PC": "PC-Kurs",
    "RE": "Religion",
    "KU": "Kunst",
    "PW": "Politik und Wirtschaft"
};
