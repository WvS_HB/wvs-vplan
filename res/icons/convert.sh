#!/bin/bash

mogrify -resize 173 -write 173.png original.png
mogrify -resize 36 -write 36-ldpi.png original.png
mogrify -resize 48 -write 48-mdpi.png original.png
mogrify -resize 114 -write 57-2x.png original.png
mogrify -resize 57 -write 57.png original.png
mogrify -resize 144 -write 72-2x.png original.png
mogrify -resize 72 -write 72-hdpi.png original.png
mogrify -resize 80 -write 80.png original.png
mogrify -resize 96 -write 96-xhdpi.png original.png
