WvS-VPlan lädt automatisch den Vertretungsplan von Moodle und parst ihn.

Bauen
=====

Siehe BUILDING:
* NodeJS installieren und mit npm Apache Cordova installieren [Infos zu NodeJS](https://nodejs.org/) [Infos zu Cordova](https://cordova.apache.org/)
* Android-Plattform installieren und zu einem neuen Cordova-Projekt hinzufügen [Infos dazu](https://developer.android.com/index.html)
* Plugins hinzufügen
* Das WvS-VPlan-Repository in den Cordova-Projektordner klonen (`git clone https://...`) [Infos dazu](https://git-scm.com/)
* mit `cordova run` die App bauen und auf dem Emulator oder am angeschlossenen Gerät starten

Editieren
=========

* In www/ liegen die Dateien für die App (index.html, js/index.js, css/index.css)
* Mit Cordova werden hybride Webanwendungen geschrieben, d. h. die App ist in Javascript und HTML5 geschrieben
* Benutzte Bibliotheken: jQuery + jQuery Mobile, PDF.js, piwik.js
* Das Projekt ist AGPLv3-lizensiert: Du darfst dir den Code ansehen, du darfst ihn bearbeiten und bauen und deine eigene Version publizieren; du musst aber den Quellcode offenlegen [Infos dazu](https://www.gnu.org/licenses/agpl-3.0.html).

Funktionsweise
==============

0. Sind aktuelle, also Pläne des heutigen Tages oder später vorhanden, werden diese aus dem Speicher angezeigt.

1. Die App loggt sich (automatisch) auf Moodle ein.
2. Die App sucht nach einem Link "Vertretungsplan".
3. Die App öffnet jeden Link auf der Planübersicht
4. und verfolgt die Weiterleitung dieser Links auf die
5. Datei (PDF), die dann heruntergeladen wird.
6. Nach dem Rendering des PDFs wird die Darstellung als Bild gespeichert.
7. Aus der PDF-Tabelle wird der Text extrahiert und als Textdatei auf dem Gerät gespeichert.
8. Der extrahierte Text wird mithilfe eines (langen!) regulären Ausdrucks abgescannt, auf der Suche nach Vertretungen von der eigenen und zusätzlich festgelegten Klassen.
9. Wurde etwas gefunden, werden alle Kürzel aufgelöst, die Tabellenzeile zu einem Text umformuliert und ein Teilen-Button generiert, eine Benachrichtigung festgelegt sowie die Vertretung über dem angezeigten Plan dargestellt.
